import os
import logging
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib import ticker
from geopy.geocoders import Nominatim
import folium

def camembert():
    df = pd.read_csv("csv/fusion_sans_doublons.csv")

    colonnes_a_garder = ["date", "type_poste"]
    donnees_propres = df[colonnes_a_garder][(df["date"]!='il y a 30 jours ') & (df["date"]!='il y a +30 jours ') & (df["type_poste"]!='Autres')]
    donnees_groupees = donnees_propres.groupby("type_poste")

    # Camembert part des emplois sur les 30 dernier jours
    df_metier= donnees_propres[['type_poste','date']].groupby(by='type_poste').count()
    df_metier.plot(kind='pie', subplots=True, figsize=(5,5), ylabel='',autopct='%1.1f%%')
    plt.legend(bbox_to_anchor=(0, 0, 0, 1))
    plt.title("Proportion d'offres dans le mois")

    return plt.show

def ratio_semaine():
    df = pd.read_csv("csv/fusion_sans_doublons.csv")
    colonnes_a_garder = ["date", "type_poste"]
    donnees_propres = df[colonnes_a_garder][(df["date"]!='il y a 30 jours ') & (df["date"]!='il y a +30 jours ') &
                                            (df["type_poste"]!='Autres')]
    donnees_propres['date'] = pd.to_datetime(donnees_propres['date'])
    donnees_propres.groupby('type_poste').resample('W-Mon', on='date').sum().sort_values(by='date')
    donnees_propres['semaine'] = donnees_propres['date'].dt.strftime('%Y/%W')
    df2 = donnees_propres[['date', 'semaine','type_poste']][donnees_propres['date']>='2021-01-01'].sort_values(by='date')

    ax = df2.groupby(['semaine','type_poste']).size().groupby(level=0).apply(
        lambda x: 100 * x / x.sum()
        ).unstack().plot(kind='bar',stacked=True, figsize=(10,7), ylabel="")

    for i, y in enumerate(ax.patches):
        if y.get_height()>0:
            ax.text(y.xy[0]+0.05,y.xy[1]+1,s =str(int(y.get_height()))+"%",fontsize=13)

    plt.legend(bbox_to_anchor=(1,1,0,0))
    plt.title("Ratio des types de poste par semaine")
    plt.gca().yaxis.set_major_formatter(ticker.PercentFormatter())
    return plt.show()

def cache():
    '''
    Utilise le cache.csv dans un dataframe nommé df si disponible sinon crée un dataframe vide.
    Ce dataframe nous servira de cache mémoire afin d'éviter de calculer les geolocalisations plusieurs fois
    evitant un temps de compilation relativement long
    '''

    if os.path.isfile("csv/cache.csv"):
        df = pd.read_csv("csv/cache.csv")
    else:
        df= pd.DataFrame()
    return df

def affichage_carto():
    '''
    Affiche une carte avec des cercles par ville en fonction du nombre d'offre par ville. Plus le cercle est
    est grand, plus il y a d'offre disponible, en passant la souris sur le cercle, vous pouvez voir le nombre
    d'offre disponible sur cette ville
    '''
    df = pd.read_csv("csv/cache.csv")
    df2 = pd.read_csv("csv/fusion_sans_doublons.csv")
    poste_groupees = df2.groupby(["type_poste"])
    geolocator = Nominatim(user_agent="thienvu")
    centre1 = geolocator.geocode("Lyon", addressdetails=True)
    carte = folium.Map(location=(centre1.latitude, centre1.longitude), zoom_start=11)

    i = 0
    for type_poste in poste_groupees.groups:
        color = ["red", "blue", "green", "purple", "orange", "darkred", "black", "darkgreen"]
        groupe = poste_groupees.get_group(type_poste)
        layer = folium.FeatureGroup(name=type_poste, show=False).add_to(carte)
        for nom_lieu in groupe["lieu"].unique():
            a = groupe["lieu"].where(groupe["lieu"] == nom_lieu).count()
            #if (nom_lieu != "le") and (nom_lieu != "st") and (nom_lieu != "rillieux") and (nom_lieu != "la"):
            if nom_lieu not in df.columns:
                print(nom_lieu)
                centre = geolocator.geocode(nom_lieu, addressdetails=True)
                try:
                    df[nom_lieu] = [centre.latitude, centre.longitude]
                except (AttributeError, KeyError):
                    print(nom_lieu,"n'a pas été détécté")
            if a <= 2 :
                cercle = 500
            elif a > 2 and a <= 10:
                cercle = 800
            elif a > 10 and a <= 50:
                cercle = 1700
            elif a > 50:
                cercle = 3000
            try:   
                folium.Circle(
                    location=df[nom_lieu],
                    radius=cercle,
                    tooltip=f"{a} offres disponible à {nom_lieu}",
                color=color[i],
                fill=True
                ).add_to(layer)
            except (AttributeError, KeyError):
                print(nom_lieu,"n'a pas été détécté")
        i += 1
        
    lieu_groupees = df2.groupby(["lieu"])    
    layer2 = folium.FeatureGroup(name="toutes les offres").add_to(carte)
    for nom_lieu in lieu_groupees.groups:
        a = df2["lieu"].where(df2["lieu"] == nom_lieu).count()
        #if (nom_lieu != "le") and (nom_lieu != "st") and (nom_lieu != "rillieux") and (nom_lieu != "la"):
        if nom_lieu not in df.columns:
            #print(nom_lieu)
            centre = geolocator.geocode(nom_lieu, addressdetails=True)
            try:
                df[nom_lieu] = [centre.latitude, centre.longitude]
            except (AttributeError, KeyError):
                    print(nom_lieu,"n'a pas été détécté")
        if a <= 2:
            cercle = 500
        elif a > 2 and a <= 10:
            cercle = 800
        elif a > 10 and a <= 50:
            cercle = 1700
        elif a > 50:
            cercle = 3000
        try:     
            folium.Circle(
                location=df[nom_lieu],
                radius=cercle,
                tooltip=f"{a} offres disponible à {nom_lieu}",
            color="crimson",
            fill=True
            ).add_to(layer2)
        except (AttributeError, KeyError):
            print(nom_lieu,"n'a pas été détécté")
            
    folium.LayerControl().add_to(carte)

    df.to_csv("csv/cache.csv")
    return carte
