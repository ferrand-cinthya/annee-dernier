import subprocess

def run_bash(cmd):
    subprocess.Popen(['/bin/bash', '-c', cmd])

run_bash('scrapy runspider scrapy_competences.py --logfile log.txt')
