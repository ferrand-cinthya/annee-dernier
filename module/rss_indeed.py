import re
import datetime
import feedparser as fp
import pandas as pd

def rss_indeed():
    """
    La fonction à partir du flux RSS de indeed créer un CSV
    """

    list_mot_cles = ['data', 'donnees']
    data = {"intitule":[],
                "nom_entreprise":[],
                "lieu":[],
                "date":[],
                "lien":[],
                "description":[]
                }

    for word in list_mot_cles:
        template = "https://fr.indeed.com/rss?q={}&l=Auvergne-Rh%C3%B4ne-Alpes"
        url = template.format(word)
        d = fp.parse(url)

        for rows in range(len(d.entries)):
            data["intitule"].append(d.entries[rows].title)
            data["lien"].append(d.entries[rows].links[0].href)

            date = d.entries[rows].published
            date = datetime.datetime.strptime(date, '%a, %d %b %Y %H:%M:%S %Z')
            data["date"].append(date.date())
            data["nom_entreprise"].append(d.entries[rows].source.title)

            lieu = d.entries[rows].title
            m = re.search(r'-(?P<nom_entreprise>.+) - (?P<Lieu>[^()]*)', lieu)
            lieu = m.group(2)

            data["lieu"].append(lieu.lower())

            data["description"].append(d.entries[rows].description)

    df = pd.DataFrame(data)
    df.to_csv("../csv/indeed.csv", index=False)
    return(df)
